import React, { Component } from 'react';
import Clipboard from 'clipboard';
import './CopyOnClick.css';

class CopyOnClick extends Component {
  state = {
    active: false
  }

  componentDidMount() {
    const { target } = this.props;

    this.clipboard = new Clipboard(this.button, {
      target: () => document.querySelector(target)
    });
  }

  componentWillUnmount() {
    this.clipboard.destroy();
  }

  showTooltip = () => {
    this.setState({ active: true });
    setTimeout(this.hideTooltip, 1000);
  }

  hideTooltip = () => {
    this.setState({ active: false });
  }

  render() {
    const { children } = this.props;
    let notification = '';

    if (this.state.active) {
      notification = <span className='CopyOnClick-notification'>Copied!</span>;
    }

    return (
      <div ref={(element) => this.button = element} onClick={this.showTooltip}>
        {children}
        {notification}
      </div>
    );
  }
}

export default CopyOnClick;
